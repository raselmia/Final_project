<?php
    ini_set('display_errors','Off');
    include_once("../../../vendor/autoload.php");
    use Rasel\Bitm\SEIP106854\TextSummary\Summary;
    
    $summary = new Summary();
    $summarys = $summary->index();
    
    
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Summary</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #utility{
                float:right;
                width:60%;
            }
        </style>
    </head>
        <?php
        include_once "../../../page/header.php";
    ?>
    <body>
     
   <div class="container">



        <h1>List Of Summary</h1>
        <div><span>Search / Filter </span> 
            <span id="utility">Download as PDF | XL  <a href="create.php">Add New</a></span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                 
                    <th>Name &dArr;</th>
                     <th>Summary list &dArr;</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               <?php
               $slno =1;
               foreach($summarys as $summary){
               ?>
                <tr>
                    <td><?php echo $slno;?></td>
                
                    <td><a href="#"><?php echo $summary->title;?></a></td>
                    <td><?php echo $summary->summary;?></td>
                    <td>View | Edit | Delete | Trash/Recover | Email to Friend </td>
                </tr>
            <?php
           $slno++;
            }
            ?>
            </tbody>
        </table>
        </div>
       
    </body>
      <ul class="pagination">
  <li><a href="#">1</a></li>
  <li class="active"><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
</ul>

        
        


    <?php include_once "../../../page/footer.php";
    ?>
</html>
