
<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');
    
use Rasel\Bitm\SEIP106854\Hobby\Hobby;
use Rasel\Bitm\SEIP106854\Utility\Utility;

$hobby = new Hobby();
$hobbys = $hobby->index();
?>




<html>
    <head>
        <title>Check Box Hobby</title>
        <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>

        <div class="container">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">

                <form class="form-inline">
                    <input type="text" class="form-control"  placeholder="Search">
                    <button type="submit" class="btn btn-default ">Search</button>
                </form>
            </div>
            <div class="container ">
                <div class="jumbotron">
                    <a href="create.php"><button class="btn btn-success">Add Hobby</button></a><a href="#"><button class="btn btn-success" style="float: right;">Dowland as PDF | XL </button></a>
                    <table class="table table-bordered table-responsive">
                        <thead>


                        <td>
                            SL
                        </td>
                        <td>Name</td>
                        <td>Hobby</td>
                        <td>Action</td>
                        </thead>
                        <tbody>
                            <?php
                            $slno = 1;
                            foreach ($hobbys as $hobby) {
                                ?>
                                <tr>
                                    <td><?php echo $slno; ?></td>
                                    <td><?php echo $hobby->name; ?></td>
                                    <td><?php echo $hobby->hoby1;echo "|-|";echo $hobby->hoby2;echo "|-|";echo  $hobby->hoby3;echo "|-|";echo  $hobby->hoby4; ?></td>
                                    <td>
                                        <a a href="show.php?id=<?php echo $hobby->id;?>" class="btn btn-success">View</a> 
                                        <a href="edit.php" class="btn btn-warning"> Edit</a> 
                                        <a href="delete.php" class="btn btn-danger">Delete</a>
                                        <a href="#" class="btn btn-warning">Trash/Recover</a>
                                        <a href="#" class="btn btn-default"> Email To Friend</a>
                                    </td>
                                </tr>
                                <?php
                                $slno++;
                            }
                            
                            ?>
                        </tbody>


                    </table>
                    <div>
                        <ul class="pagination">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>