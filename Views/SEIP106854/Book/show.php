<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \Rasel\BITM\SEIP106854\Book\Book;
use  \App\BITM\SEIP1020\Utility\Utility;

$book = new Book();
$book = $book->show($_GET['id']);

//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>show book</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
    </head>
     <?php 



       include_once "../../../page/header.php";




    ?>
    <body>
        <div class="container">
<h1>Book Detail</h1>

<dl>
    <dt>Id</dt>
    <dd><?php echo $book['id']; ?></dd>
    
    <dt>Title</dt>
    <dd><?php echo $book['title']; ?></dd>
    
    <dt>Author</dt>
    <dd><?php echo $book['author']; ?></dd>
</dl>

<nav>
    <li><a href="index.php">Go to list</a></li>
</nav>

    </body>
</div>
                  <?php
include_once "../../../page/footer.php";


              ?>
</html>