<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject1'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use \Rasel\BITM\SEIP106854\Date\Birthday;
use  \Rasel\BITM\SEIP106854\Utility\Utility;

$birthday = new Birthday();
$birthday = $birthday->show($_GET['id']);

//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>View the Birthday </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link rel="stylesheet" href="../../../style.css">
           <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
    </head>
     <?php 



       include_once "../../../page/header.php";




    ?>
    <body>
        <div class="container">
<h1>Birthday Details</h1>
<table class="table table-bordered">
<dl>
    <dt>Id</dt>
    <dd><?php echo $birthday['id']; ?></dd>
    
    <dt>Name</dt>
    <dd><?php echo $birthday['name']; ?></dd>
    
    <dt>Birthday Date</dt>
    <dd><?php echo $birthday['date']; ?></dd>
</dl>
    </table>

<nav>
    <li><a href="index.php">Go to list</a></li>
</nav>
</div>

    </body>
    
              <?php
include_once "../../../page/footer.php";


              ?>
</html>